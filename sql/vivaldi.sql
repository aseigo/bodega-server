--begin;

insert into partners (name, developer, distributor) values ('KDE', true, true);

insert into devices (partner, name, description, partnumber)
       values (0, 'Vivaldi', 'Plasma Active tablet from Make Play Live', 'VIVALDI-1');

--end;
